renderJobsToDom();
getJobFromLS();

//POST A JOB PAGE

let form = document.querySelector("form");
let input = document.querySelector("#inputOne");
let myLocation = document.querySelector("#inputLocation");
let desc = document.querySelector("#inputDesc");
let company = document.querySelector("#inputCompany");
// let btn = document.querySelector("#button");

// Eventlisteners
form.addEventListener("submit", createJob);


function deleteJob(e){
    e.preventDefault();
    let currentTarget = e.target;
    delFromLS(currentTarget.parentElement.id);
    displayNoJob();
    deleteFromDom(currentTarget);
 
}

function deleteFromDom(target) {
    const parent = target.parentElement.parentElement;
    const mainParent = parent.parentNode;
    mainParent.removeChild(parent);
    
}


function createJob(e) {
    //create list element
    e.preventDefault();
    const jobObject = {
        jobId: new Date().getTime(),
        jobTitle: input.value,
        jobLocation: myLocation.value,
        jobCompany: company.value,
        jobDesc: desc.value
    };

    //add job to locale storage
    addJobToLS(jobObject);
    renderJobsToDom();
   
     input.value = "";
     myLocation.value = "";
     company.value = "";
     desc.value = "";
}

function addJobToLS(createdjob) {
    const allJobs = getJobFromLS();
    allJobs.push(createdjob);
    localStorage.setItem("jobs", JSON.stringify(allJobs));
}

function delFromLS(id) {
    const jobs = getJobFromLS();
   
    if (jobs) {
        if(Array.isArray(jobs) && jobs.length > 0){
          
            let newJobs = jobs.filter(job => {
                // console.log("JOB IDDDD",job.jobId, typeof job.jobId);
                // console.log(
                //     "ID",id, typeof id);
                return parseInt(job.jobId) !== parseInt(id);
            });

            localStorage.setItem("jobs", JSON.stringify(newJobs));
        }
    }

}
