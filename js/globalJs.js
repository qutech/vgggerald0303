function createJobComponent(job) {
    const li = document.createElement("li");
    li.classList.add("list");
    let location = window.location.href;
    let page  = location.substring(location.lastIndexOf('/'), location.length);
// console.log(page);
    if (page === "/new-post.html") {
      //create remove button
      const removeBtn = document.createElement("a");
      removeBtn.classList = "remove-job";
      removeBtn.innerHTML = `<i class="far fa-window-close"></i>`;
      removeBtn.id = job.jobId;
      removeBtn.addEventListener("click", deleteJob);
      li.appendChild(removeBtn);
    }
   
 const jobSpan = document.createElement("span");
 jobSpan.classList = "span-job"
 jobSpan.innerHTML = '<i class="fas fa-suitcase"></i>';
//  li.appendChild(jobSpan);

    const jobTitle = document.createElement("p");
    jobTitle.classList = "jt";
    jobTitle.textContent = job.jobTitle;

 const jobDiv = document.createElement("div");
 jobDiv.classList = "div-job";
 jobDiv.appendChild(jobSpan);
 jobDiv.appendChild(jobTitle);
    li.appendChild(jobDiv);

    const comP = document.createElement("p");
    const comSpan = document.createElement('span');
    comSpan.classList = "span-com";
    comSpan.innerHTML = '<i class="far fa-building"></i>';
    li.appendChild(comSpan);
    comP.classList = 'com';
    comP.textContent = job.jobCompany;

     const comDiv = document.createElement("div");
     comDiv.classList = "div-com";
     comDiv.appendChild(comSpan);
     comDiv.appendChild(comP);
    li.appendChild(comDiv);



    const locP = document.createElement("p");
    const locSpan = document.createElement("span");
    locSpan.classList = 'span-loc';
    locSpan.innerHTML = '<i class="fas fa-map-marker-alt"></i>';
    li.appendChild(locSpan);
    locP.classList = 'loc';
    locP.textContent = job.jobLocation;
    li.appendChild(locP);

      const locDiv = document.createElement("div");
      locDiv.classList = "div-loc";
      locDiv.appendChild(locSpan);
      locDiv.appendChild(locP);
      li.appendChild(locDiv);


    const descP = document.createElement("p");
    descP.classList = 'des'
    descP.textContent = job.jobDesc.substring(0, 45);
    li.appendChild(descP);

     const viewJobDetails = document.createElement("a");
     viewJobDetails.classList = "view-job";
     viewJobDetails.innerHTML = `<a href="job-single.html?postId=${job.jobId}" class="vj">View Job</a>`;
    //  viewJobDetails.href = `job-single.html`;
     viewJobDetails.id = job.jobId;

     li.appendChild(viewJobDetails);

    return li;
}

const jobList = document.querySelector("#jobsList");

function renderJobsToDom() {

    jobList.innerHTML = "";
    const allJobs = getJobFromLS();
    console.log(allJobs);
    if(allJobs ){
    if (Array.isArray(allJobs) && allJobs.length > 0) {
        
        allJobs.forEach(job => {
        const jobComponent = createJobComponent(job);
        jobList.append(jobComponent);
        });
    }

    }
    // getSearchResults();
    // filteredSearch(allJobs);
displayNoJob();
}



function getJobFromLS() {
    // check if jobs exists and add jobs to local storage
    let jobsLS = localStorage.getItem("jobs");
    if (!jobsLS) {
        return [];
    } else {
        return JSON.parse(jobsLS);
    }
}


function displayNoJob() {
  const allJobs = getJobFromLS();

  if (allJobs.length === 0) {
    document.querySelector("#zero-jobs").innerHTML =
      "<p>No jobs in the database</p>";
  } else if (allJobs.length > 0) {
    document.querySelector(
      "#zero-jobs"
    ).innerHTML = `<p>${allJobs.length} jobs available</p>`;
  }
}



//VIEW A JOB PAGE LOGIC


// Parse url parameter to get the
let queryParameters = location.search;
let sanitizedQP = queryParameters.substring(1); // Remove question mark(?) from url search
let sanitizedQPArray = sanitizedQP.split("&");
params = {};
sanitizedQPArray.forEach(function(sanitizedQP) {
  paramsKeyValueArray = sanitizedQP.split("=");
  params[paramsKeyValueArray[0]] = paramsKeyValueArray[1];
});
// console.log(params);

//VIEW A JOB PAGE LOGIC
const viewaJobPage = document.querySelector("#viewaJobPage");

(async function viewJobPage(postId) {
    const allJobs = getJobFromLS();

    allJobs.forEach((job)=>{

      if (job.jobId == postId){
        console.log('thesame')
      
      console.log(job);

         viewaJobPage.innerHTML += `
      <div class="col-md-12 col-lg-12 mb-5">

            
              <div class="p-5 bg-white">

              <div class="mb-4 mb-md-5 mr-5">
               <div class="job-post-item-header d-flex align-items-center">
               
                 <h2 class="mr-3 text-black h4"><i class="fas fa-suitcase suit"></i>${job.jobTitle}</h2>
                 <div class="badge-wrap">
                  <span class="border border-warning text-warning py-2 px-4 rounded">Freelance</span>
                 </div>
               </div>
               <div class="job-post-item-body d-block d-md-flex">
              
                 <div class="mr-3"><span class="fl-bigmug-line-portfolio23"></span> <a href="#"><i class="fas fa-map-marker-alt mark"></i>${job.jobLocation}</a></div>
                  <div><span class="fl-bigmug-line-big104"></span> <span><i class="far fa-building build"></i>${job.jobCompany}</span></div>
               

               </div>
              </div>
              
              <p>${job.jobDesc}</p>
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <h2 style="text-align: center">APPLY HERE</h2>
    <p style="font-weight: 500; font-size:1rem; text-align: center">Send resumes to careers@WorkiLinks.com</p>
  </div>

</div>
              <p class="mt-5 view-job"><a href="#" class="btn btn-primary  py-2 px-4">Apply Job</a></p>
            </div>
          </div>
            `;
      }
    });

    
  }
)(params.postId);
// viewJobPage(params.postId);
// viewJobPage();

//APPLY HERE 



var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.querySelector(".view-job");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function(e) {
  e.preventDefault();
  modal.style.display = "block";
 };

// When the user clicks on <span> (x), close the modal
span.onclick = function(e) {
  e.preventDefault();
  modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};
