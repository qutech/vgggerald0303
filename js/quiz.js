// const questions = require('./questions');
const quizContainer = document.getElementById("quiz");
const resultsContainer = document.getElementById("results");
const submitButton = document.getElementById("submit");


const myQuestions = [
  {
    question: "Book is to Reading as Fork is to?",
    answers: {
      a: "Drawing",
      b: "Writing",
      c: "Eating"
    },
    correctAnswer: "c"
  },
  {
    question: "What is the best site ever created?",
    answers: {
      a: "SitePoint",
      b: "Simple Steps Code",
      c: "Trick question; they're both the best"
    },
    correctAnswer: "c"
  },
  {
    question:
      "Which of the following can be arranged into a 5-letter English word?",
    answers: {
      a: "H R G S T",
      b: "T O O M T",
      c: "R I L S A"
    },
    correctAnswer: "c"
  },
  {
    question: "What number best completes the analogy: 8:4 as 10?",
    answers: {
      a: "5",
      b: "9",
      c: "6"
    },
    correctAnswer: "a"
  },
  {
    question: "PEACH is to HCAEP as 46251 is to?",
    answers: {
      a: "26451",
      b: "15264",
      c: "55111"
    },
    correctAnswer: "b"
  },
  {
    question: "Finger is to Hand as Leaf is to",
    answers: {
      a: "Tree",
      b: "Bark",
      c: "Branch",
      d: "Twig"
    },
    correctAnswer: "d"
  },
  {
    question: "Choose the number that is 1/4 of 1/2 of 1/5 of 200:",
    answers: {
      a: "3",
      b: "10",
      c: "5"
    },
    correctAnswer: "c"
  },
  {
    question: "What is 10/2*3?",
    answers: {
      a: "12",
      b: "16",
      c: "15"
    },
    correctAnswer: "c"
  },
  {
    question: "Choose the word most similar to Trustworthy",
    answers: {
      a: "Tenacity",
      b: "Relevance",
      c: "Insolence",
      d: "Reliable"
    },
    correctAnswer: "d"
  },
  {
    question: "Who is the strongest?",
    answers: {
      a: "Superman",
      b: "The Terminator",
      c: "Waluigi, obviously"
    },
    correctAnswer: "c"
  }
];

function buildQuiz() {
  // we'll need a place to store the HTML output
  const output = [];

  // for each question...
  myQuestions.forEach((currentQuestion, questionNumber) => {
    //   console.log(questionNumber);
    // we'll want to store the list of answer choices
    // console.log(currentQuestion.answers);

    const answers = [];

    // and for each available answer...
    for (letter in currentQuestion.answers) {
//    console.log(currentQuestion.answers[letter]);
      // ...add an HTML radio button
      answers.push(
        `<label>
            <input type="radio" name="question${questionNumber}" value="${letter}"> ${letter} :
             ${currentQuestion.answers[letter]}
          </label>`
      );
    }
    // add this question and its answers to the output
   output.push(
     `<div class="slide">
    <div class="question"> ${currentQuestion.question} </div>
    <div class="answers"> ${answers.join("")} </div>
  </div>`
   );
  });
// console.log(output);
  // finally combine our output list into one string of HTML and put it on the page
  quizContainer.innerHTML = output.join("");
}


function showResults() {
  // gather answer containers from our quiz
  const answerContainers = quizContainer.querySelectorAll(".answers");

  // keep track of user's answers
  let numCorrect = 0;

  // for each question...
  myQuestions.forEach((currentQuestion, questionNumber) => {
    // find selected answer
    const answerContainer = answerContainers[questionNumber];
    const selector = "input[name=question" + questionNumber + "]:checked";
       

    const userAnswer = (answerContainer.querySelector(selector) || {}).value;

    // if answer is correct
    if (userAnswer === currentQuestion.correctAnswer) {
        
      // add to the number of correct answers
      numCorrect++;

      // color the answers green
      answerContainers[questionNumber].style.color = "lightgreen";
    }
    // if answer is wrong or blank
    else {
      // color the answers red
      answerContainers[questionNumber].style.color = "red";
    }
  });

  // show number of correct answers out of total
  // resultsContainer.innerHTML = numCorrect + " out of " + myQuestions.length;
  if(numCorrect <= 5){
  resultsContainer.innerHTML = `You scored ${numCorrect} out of ${myQuestions.length}. Please retake the quiz at a later time`;

}else{
  resultsContainer.innerHTML = `You scored ${numCorrect} out of ${myQuestions.length}. You are a Quiz Ninja!!`;
}


}

//TIMER
function countdownTimer(duration, display) {
  var timer = duration;
  var minutes;
  var seconds;
  var intervalId;
  intervalId = setInterval(function() {
    minutes = parseInt(timer / 60, 10);
    seconds = parseInt(timer % 60, 10);

    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    display.textContent = minutes + ":" + seconds;

    // if (--timer < 0) {
    //   timer = duration;
    // }
    if (--timer < 0) {
      clearInterval(intervalId);
      showResults();
    }
  }, 1000);
}

window.onload = function() {
  var threeMinutes = 60,
    display = document.querySelector("#timer");
  countdownTimer(threeMinutes, display);
};


// display quiz right away
 buildQuiz();

// on submit, show results
submitButton.addEventListener("click", showResults);



/* PAGINATION */

const previousButton = document.getElementById("previous");
const nextButton = document.getElementById("next");
const slides = document.querySelectorAll(".slide");
let currentSlide = 0;

function showSlide(n){
  slides[currentSlide].classList.remove('active-slide');
  slides[n].classList.add("active-slide");
  currentSlide = n;

  if (currentSlide === 0) {
    previousButton.style.display = "none";
  } else {
    previousButton.style.display = "inline-block";
  }
 
 if (currentSlide === slides.length - 1) {
   nextButton.style.display = "none";
   submitButton.style.display = "inline-block";
 } else {
   nextButton.style.display = "inline-block";
   submitButton.style.display = "none";
 }
}

showSlide(0);

function showNextSlide() {
  showSlide(currentSlide + 1);
}

function showPreviousSlide() {
  showSlide(currentSlide - 1);
}

previousButton.addEventListener("click", showPreviousSlide);
nextButton.addEventListener("click", showNextSlide);






// numCorrect <= 5 ? resultsContainer.innerHTML = `You scored ${numCorrect} out of ${myQuestions.length}. Please retake the quiz at a later time` : resultsContainer.innerHTML = `You scored ${numCorrect} out of ${myQuestions.length}. You are a Quiz Ninja!!`
// display quiz right away

// if(numCorrect <= 5){
//   resultsContainer.innerHTML = `You scored ${numCorrect} out of ${myQuestions.length}. Please retake the quiz at a later time`;

// }else{
//   resultsContainer.innerHTML = `You scored ${numCorrect} out of ${myQuestions.length}. You are a Quiz Ninja!!`;
// }