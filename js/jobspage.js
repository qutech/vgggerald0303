renderJobsToDom();
// displayNoJob();
const searchTitleLocation = document.querySelector("form");
searchTitleLocation.addEventListener("submit", searchJob);

function searchJob(e) {
  // getSearchResults();
  e.preventDefault();
  let jobTitles = document.querySelector("#findjobtitle").value.trim();
  let jobLocations = document.querySelector("#findjoblocation").value.trim();
  let noJob = document.querySelector("#no-jobs");
  // console.log(jobLocations);
  const filteredJobs = [];

  if ((jobTitles || jobTitles != "") && (jobLocations || jobLocations != "")) {
    const allJobs = getJobFromLS();
    if (allJobs.length > 0) {
      for (let i in allJobs) {
        let currentJob = allJobs[i];
        if (
          currentJob.jobTitle.toLowerCase().includes(jobTitles.toLowerCase()) &&
          currentJob.jobLocation
            .toLowerCase()
            .includes(jobLocations.toLowerCase())
        ) {
          filteredJobs.push(currentJob);
          console.log(filteredJobs);
          //additionaal logic to render filtered jobs
          // const searchResults = localStorage.setItem("searchresult", JSON.stringify(filteredJobs));
          getSearchResults(filteredJobs);
        } else {
          // continue;
          noJob.innerHTML = `<p class="no-jobs-found">${filteredJobs.length} jobs found</p>`;
          document.querySelector("#jobsList").style.display = "none";
          document.querySelector("#zero-jobs").style.display = "none";


          // document.querySelector("#jobsCount").style.display = "none";
        }
        noJob.innerHTML = `<p class="no-jobs-found">${filteredJobs.length} jobs found</p>`;
      }

          // noJob.innerHTML = `<p class="no-jobs-found">${filteredJobs.length === 1} ? 1 job found: 
          // ${filteredJobs.length} jobs found</p>`;
    }

    // else {
    //   alert("No job in the database");
    // }
  } else {
    // alert("invalid input")
          noJob.innerHTML = `<p class="no-jobs-found">${filteredJobs.length} jobs found</p>`;
          document.querySelector("#jobsList").style.display = "none";
          document.querySelector("#zero-jobs").style.display = "none";  }
  jobTitles = "";
  jobLocations = "";
}


const jobsListing = document.querySelector("#jobsList");
const jobsCount = document.querySelector("#jobsCount");
const searchss = JSON.parse(localStorage.getItem("jobs"));
// getSearchResults(searchJob)
function getSearchResults(search) {
  
  if (search.length > 0) {
    // search !== null &&
    let list = "";
    search.forEach(filteredSearch => {
      list += `<li class="list">
      <div className="div-job">
      <span className="span-job">
      <i class="fas fa-suitcase"></i>
      </span>
      <p class="jt">${filteredSearch.jobTitle}</p>
      </div>

      <div class="div-com">
      <span class="span-com">
      <i class="far fa-building"></i>
      </span>
      <p class="com">${filteredSearch.jobCompany}</p></div>

      <div class="div-loc"><span class="span-loc"><i class="fas fa-map-marker-alt"></i></span><p class="loc">${filteredSearch.jobLocation}</p></div>

      <p class="des">${filteredSearch.jobDesc}</p>
  <a class="view-job" href="job-single.html?postId=${filteredSearch.jobId}"><p class="vj">Apply Here</p></a>
      </li>`;
      jobsCount.innerHTML = list;
    });
  } else {
    jobsCount.innerHTML = `<div class="single-post d-flex flex-row"> <div class="titles">
 <h4>No Jobs Found</h4>
 </div></div>`;
  }

  document.querySelector("#jobsList").style.display = "none";
  document.querySelector("#zero-jobs").style.display = "none";
  // viewJobPage(jobsCount);
}

// //VIEW A JOB PAGE LOGIC


