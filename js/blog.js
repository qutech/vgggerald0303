const postList = document.getElementById("post");
const title = document.getElementById("inputTitle");
const post = document.getElementById("inputPost");
let form = document.querySelector("form");
const postComment = document.getElementById("comments");
const postCommentList = document.getElementById("postCommentList");

// const date = new Date().toUTCString();
const date = new Date().toUTCString();
const img1 = "../images/img_1.jpg";
const img2 = "../images/img_2.jpg";
const img3 = "../images/img_3.jpg";
const img4 = "../images/img_4.jpg";

const allImg = [img1, img2, img3, img4];
let randomImage = Math.floor(Math.random() * allImg.length);
const oneimg = allImg[randomImage];


//paginate

// (async function displayPosts() {

//       const url = await fetch(
//         "https://jsonplaceholder.typicode.com/posts?_start=0&_limit=8"
//       );
//       const data = await url.json();

//       data.forEach(post => {
//         postList.innerHTML += `

//             <div class="col-md-3 mb-5 mb-lg-4 col-lg-3 aos-init aos-animate" data-aos="fade">
//               <div class="position-relative unit-8">
//               <a href="comments.html?postId=${post.id}" class="mb-3 d-block img-a"><img src=${oneimg} alt="Image" class="img-fluid rounded"></a>
//               <span class="d-block text-gray-500 text-normal small mb-3">Posted on <a href="#">${date}</a>

//               <h2 class="h5 font-weihgt-normal line-height-sm mb-3"><a href="#" class="text-black">${post.title}</a></h2>
//               <p>${post.body}</p>
//               </div>
//    </div>
//             `;
//       });

// })();

// displayPosts();






(async function displayPosts() {
  try {
    const url = await fetch("https://jsonplaceholder.typicode.com/posts");
    const data = await url.json();

    var current_page = 1;
    var records_per_page = 8;
    function prevPage() {
      if (current_page > 1) {
        current_page--;
        changePage(current_page);
      }
      document.documentElement.scrollTo({ top: 0, behavior: "smooth" });
    }

    function nextPage() {
      if (current_page < numPages()) {
        current_page++;
        changePage(current_page);
      }
      document.documentElement.scrollTo({ top: 0, behavior: "smooth" });
    }

    function changePage(page) {
      var btn_next = document.getElementById("front-arrow");
      btn_next.addEventListener("click", nextPage);
      var btn_prev = document.getElementById("back-arrow");
      btn_prev.addEventListener("click", prevPage);
      const postList = document.getElementById("post");
      var page_span = document.getElementById("page");

      // Validate page
      if (page < 1) page = 1;
      if (page > numPages()) page = numPages();

      postList.innerHTML = "";

      for (
        var i = (page - 1) * records_per_page;
        i < page * records_per_page && i < data.length;
        i++
      ) {
        console.log(data[i]);
        postList.innerHTML += `

            <div class="col-md-3 mb-5 mb-lg-4 col-lg-3 aos-init aos-animate" data-aos="fade">
              <div class="position-relative unit-8">
              <a href="comments.html?postId=${data[i].id}" class="mb-3 d-block img-a"><img src=${oneimg} alt="Image" class="img-fluid rounded"></a>
              <span class="d-block text-gray-500 text-normal small mb-3">Posted on <a href="#">${date}</a>

              <h2 class="h5 font-weihgt-normal line-height-sm mb-3"><a href="#" class="text-black">${data[i].title}</a></h2>
              <p>${data[i].body}</p>
              </div>
   </div>
            `;
      }
      page_span.innerHTML = page + "/" + numPages();
    }

    function numPages() {
      return Math.ceil(data.length / records_per_page);
    }

    window.onload = function() {
      changePage(1);
    };
  } catch (error) {
    console.log(error);
  }
})();


// Parse url parameter to get the
let queryParameters = location.search;
let sanitizedQP = queryParameters.substring(1); // Remove question mark(?) from url search
let sanitizedQPArray = sanitizedQP.split("&");
params = {};
sanitizedQPArray.forEach(function(sanitizedQP) {
  paramsKeyValueArray = sanitizedQP.split("=");
  params[paramsKeyValueArray[0]] = paramsKeyValueArray[1];
});
console.log(params);



(async function postsComments(postId) {
  try {
    const postUrl = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${postId}`
    );
    const postData = await postUrl.json();

    postCommentList.innerHTML = `
            <div class="col-lg-12 mb-5 mb-lg-4 col-lg-3 aos-init aos-animate" data-aos="fade">
              <div class="position-relative unit-8">
              <p class="mb-3 d-block img-a"><img src=${oneimg} alt="Image" class="img-fluid rounded" style="height:10rem"></p>
              <h2 class="h5 font-weihgt-normal line-height-sm mb-3"><a href="#" class="text-black">${postData.title}</a></h2>
              <p>${postData.body}</p>
              </div>
   </div>
            `;

    const url = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${postId}/comments?postId=${postId}`
    );
    const data = await url.json();

    data.forEach(comment => {
      postComment.innerHTML += `
            <div style="border: 1px solid grey" class="col-md-12 col-lg-12  col-sm-12 mb-5 mb-lg-4 col-lg-3 aos-init aos-animate" data-aos="fade">
              <div class="position-relative unit-8">
              <span class="d-block text-gray-500 text-normal small mb-3">By <a href="#">${comment.email}</a>
         
              <h2 class="h5 font-weihgt-normal line-height-sm mb-3"><a href="#" class="text-black">${comment.name}</a></h2>
              <p>${comment.body}</p>
              </div>
   </div>
            `;
    });
  } catch (error) {
    console.log(error);
  }
})(params.postId);
// postsComments(params.postId);
// postsComments();

//Paginate
